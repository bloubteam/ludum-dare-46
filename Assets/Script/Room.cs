﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room : MonoBehaviour
{
    SpriteMask roomMask;

    // Start is called before the first frame update
    void Start()
    {
        roomMask = GetComponentInChildren<SpriteMask>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        roomMask.gameObject.SetActive(false);
    }

    void OnTriggerExit2D(Collider2D collider)
    {
        roomMask.gameObject.SetActive(true);
    }
}
