﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TalkingInterface : MonoBehaviour
{
    [Serializable]
    public struct TextAndSound
    {
        public string text;
        public AudioClip sound;
    }

    public Text text;
    public string[] fridgeTexts;
    public AudioClip fridgeSound;
    public TextAndSound[] ItSounds;

    private Animation anim;
    private AudioSource sound;

    void RunTextAndSound(TextAndSound textAndSound)
    {
        text.text = textAndSound.text;
        sound.PlayOneShot(textAndSound.sound);
    }

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animation>();
        sound = GetComponent<AudioSource>();
    }

    public void TakeFood()
    {
        anim.Play("FoodTaking");
    }

    public void FeedIt()
    {
        anim.Play("MonsterTalking");
    }

    void PlayerTalking()
    {
        text.text = "Je suis le player.";
    }

    void ItTalking()
    {
        RunTextAndSound(ItSounds[UnityEngine.Random.Range(0, ItSounds.Length)]);
    }

    void OpenFridge()
    {
        text.text = fridgeTexts[UnityEngine.Random.Range(0, fridgeTexts.Length)];
        sound.PlayOneShot(fridgeSound);
    }

    void StartItTalking()
    {
        GameManager.Instance.StartGame();
        text.text = "I'm really HUNGRYYYY if you don't feed me, I will DESTROY THE WORLD.";
    }

    void StartPlayerTalking1()
    {
        text.text = "Ho Scrap ! It Finally woke up... What a chance that it's the \"Last day of Humannity on earth (©Evan Mosk)\" and everybody is on the way to go in space !";
    }

    void StartPlayerTalking2()
    {
        text.text = "Maybe I have something to feed It and let Humanity run away !\nLet's see in my fridge, maybe there are some kid's corpses left.";
    }

    void SpawnIt()
    {
        GameManager.Instance.FindWithTag("ItMonster").GetComponent<ItMonster>().Respawn();
    }
}
