﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraBehaviour : MonoBehaviour
{
    public Transform target;

    public bool ActivateExperimentalSmoothness = false;
    public float LerpSmoothness = 1f;

    public bool HardFollow = false;

    public Vector2 bounds;
    public Vector2 offset;
    public bool showBounds;

    private Vector2 targetPos;
    private float cameraDepth;

    // Start is called before the first frame update
    void Start()
    {
        target = GameManager.Instance.FindWithTag("Player").transform;
        targetPos = target.position;
        cameraDepth = this.transform.position.z;
        this.transform.position = new Vector3(targetPos.x, targetPos.y, cameraDepth);
    }

    // Update is called once per frame
    void Update()
    {
        targetPos = target.position;

        if (HardFollow)
        {
            this.transform.position = new Vector3(targetPos.x, targetPos.y, cameraDepth);
            return;
        }

        Vector2 pos = new Vector2(this.transform.position.x, this.transform.position.y) + offset;
        Vector2 newPos = this.transform.position;

        if (targetPos.x > (pos.x + (bounds.x / 2.0f)))
            newPos.x += targetPos.x - (pos.x + (bounds.x / 2.0f));
        if (targetPos.x < (pos.x - (bounds.x / 2.0f)))
            newPos.x += targetPos.x - (pos.x - (bounds.x / 2.0f));

        if (targetPos.y > (pos.y + (bounds.y / 2.0f)))
            newPos.y += targetPos.y - (pos.y + (bounds.y / 2.0f));
        if (targetPos.y < (pos.y - (bounds.y / 2.0f)))
            newPos.y += targetPos.y - (pos.y - (bounds.y / 2.0f));

        if (ActivateExperimentalSmoothness)
            this.transform.position = Vector3.Lerp(this.transform.position, new Vector3(newPos.x, newPos.y, cameraDepth), LerpSmoothness * Time.deltaTime);
        else
            this.transform.position = new Vector3(newPos.x, newPos.y, cameraDepth);
    }

    private void OnDrawGizmos()
    {
        if (!showBounds)
            return;

        Gizmos.color = Color.green;

        Vector2 pos = new Vector2(this.transform.position.x, this.transform.position.y) + offset;
        Vector2 upLeft = new Vector2(pos.x - (bounds.x / 2.0f), pos.y + (bounds.y / 2.0f));
        Vector2 upRight = new Vector2(pos.x + (bounds.x / 2.0f), pos.y + (bounds.y / 2.0f)); ;
        Vector2 downLeft = new Vector2(pos.x - (bounds.x / 2.0f), pos.y - (bounds.y / 2.0f)); ;
        Vector2 downRight = new Vector2(pos.x + (bounds.x / 2.0f), pos.y - (bounds.y / 2.0f)); ;

        Gizmos.DrawLine(upLeft, upRight);
        Gizmos.DrawLine(upRight, downRight);
        Gizmos.DrawLine(downRight, downLeft);
        Gizmos.DrawLine(downLeft, upLeft);
    }
}
