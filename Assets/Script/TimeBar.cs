﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode()]
public class TimeBar : MonoBehaviour
{
    public int maximum;
    public Image mask;
    public Image color;
    private GameObject It;
    private int current;


    // Start is called before the first frame update
    void Start()
    {
      It = GameManager.Instance.FindWithTag("ItMonster");
      int starvation = It.GetComponent<ItMonster>().GetStarvation();
      SetCurrent(starvation);
    }

    // Update is called once per frame
    void Update()
    {
        GetCurrentFilled();
        It = GameManager.Instance.FindWithTag("ItMonster");
        if (current > 0)
          SetCurrent(It.GetComponent<ItMonster>().GetStarvation());
        if (current > 50 && current <= 75)
          color.color=new Color32(255,255,0,204);
        else if (current > 25 && current <= 50)
          color.color=new Color32(255,150,0,204);
        else if (current > 0 && current <= 25)
          color.color=new Color32(255,0,0,204);
        else
            color.color = new Color32(0, 255, 0, 204);
    }

    void GetCurrentFilled()
    {
      float fillAmountC = (float)current / (float)maximum;
      mask.fillAmount = fillAmountC;
    }

    private void SetCurrent(int value)
    {
      current = value;
    }

    public int GetCurrentValue()
    {
      Debug.Log("Im' In function !");
      return (current);
    }

    public void AddCurrentValue(int value)
    {
      current = current + value;
    }
}
