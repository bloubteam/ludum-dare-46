﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOver : MonoBehaviour
{
    public AudioClip winSound;
    public AudioClip looseSound;
    public Sprite winSprite;
    public Sprite looseSprite;
    public Image image;
   
    private AudioSource music;

    // Start is called before the first frame update
    void Start()
    {
        music = GetComponent<AudioSource>();
        if (GameManager.Instance.getSavedPeople() >= 7594325677)
        {
            music.PlayOneShot(winSound);
            image.overrideSprite = winSprite;
        }
        else
        {
            music.PlayOneShot(looseSound);
            image.overrideSprite = looseSprite;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
