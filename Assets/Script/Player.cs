﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public float speed = 110;

    private Rigidbody2D rb2d;
    private bool haveFood = false;
    
    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Vector2 move = new Vector2();

        move.x = Input.GetAxis("Horizontal");
        move.y = Input.GetAxis("Vertical");

        if (move != Vector2.zero)
        {
            move.Normalize();
            transform.right = move;
            move = move * speed * Time.deltaTime;
        }
        rb2d.MovePosition(rb2d.position + move);
    }

    public bool TakeFood()
    {
        Debug.Log("Player take food");
        if (haveFood)
            return false;
        haveFood = true;
        return true;
    }

    public bool GiveFood()
    {
        Debug.Log("Player give food");
        if (haveFood)
        {
            haveFood = false;
            return (true);
        }
        return (false);
    }
}
