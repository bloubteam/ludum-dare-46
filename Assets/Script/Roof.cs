﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Roof : MonoBehaviour
{
    private Animation anim;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animation>();
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        Player player = (Player)collider.GetComponent<Player>();

        if (player != null)
        {
            anim.Play("Fade_out");
        }
    }
    private void OnTriggerExit2D(Collider2D collider)
    {
        Player player = (Player)collider.GetComponent<Player>();

        if (player != null)
        {
            anim.Play("Fade_in");
        }
    }

}
