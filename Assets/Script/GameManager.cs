﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : Singleton<GameManager>
{
    public string startScene;

    public long peopleSaved;

    private Dictionary<string, GameObject> gameObjectFound = new Dictionary<string, GameObject>();
    private Dictionary<string, GameObject> gameObjectFoundWithTag = new Dictionary<string, GameObject>();

    private bool gameRuning = false;

    protected virtual void Start()
	{
        if (!string.IsNullOrEmpty(startScene))
            ChangeScene(startScene);
    }

    void Update()
    {
        if (gameRuning)
        {
            peopleSaved += System.Convert.ToInt64((7594325677 / 900) * Time.deltaTime);
            if (peopleSaved >= 7594325677)
                GameOver();
        }
    }

    public void ChangeScene(string sceneName)
    {
        Debug.Log("Change scene to " + sceneName);
        if (sceneName == "MainLevel")
        {
            peopleSaved = 0;
            gameRuning = true;
            Debug.Log("gameRuning : " + gameRuning);
        }
        else
            gameRuning = false;
        SceneManager.LoadScene(sceneName);
    }

    public void StartGame()
    {
        gameRuning = true;
    }

    public void GameOver()
    {
        ChangeScene("GameOver");
    }

    public void QuitGame()
    {
        Debug.Log("Quit Game");
        Application.Quit();
    }

    public long getSavedPeople()
    {
        return peopleSaved;
    }

    public GameObject Find(string name)
    {
        GameObject gameObject;
        if (gameObjectFound.TryGetValue(name, out gameObject) && gameObject != null)
            return gameObject;
        gameObjectFound[name] = GameObject.Find(name);
        return gameObjectFound[name];
    }

    public GameObject FindWithTag(string name)
    {
        GameObject gameObject;
        if (gameObjectFoundWithTag.TryGetValue(name, out gameObject) && gameObject != null)
            return gameObject;
        gameObjectFoundWithTag[name] = GameObject.FindWithTag(name);
        return gameObjectFoundWithTag[name];
    }
}
