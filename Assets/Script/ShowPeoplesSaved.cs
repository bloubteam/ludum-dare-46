﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowPeoplesSaved : MonoBehaviour
{
    public string prefix;
    public string suffix;

    private Text textComponent;
    void Start()
    {
        textComponent = GetComponent<Text>();
    }
    void Update()
    {
        textComponent.text = prefix + System.String.Format("{0:n0}", GameManager.Instance.getSavedPeople()) + suffix;
    }
}
