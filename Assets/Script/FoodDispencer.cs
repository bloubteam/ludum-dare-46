﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodDispencer : MonoBehaviour
{
    void Start()
    {
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        Player player = (Player)collider.GetComponent<Player>();

        if (player != null)
        {
            if (player.TakeFood())
            {
                GameManager.Instance.FindWithTag("TalkingInterface").GetComponent<TalkingInterface>().TakeFood();
            }
        }
    }
}
