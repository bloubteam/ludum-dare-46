﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItMonster : MonoBehaviour
{
    public float Starvation = 100f;
    public GameObject SpawnArea;

    CompositeCollider2D SpawnCollider;
    float HungerSpeed = 1f;
    int distance_max = 10;

    // Start is called before the first frame update
    void Start()
    {
        SpawnCollider = SpawnArea.GetComponent<CompositeCollider2D>();
    }

    // Update is called once per frame
    void Update()
    {
       HungerGrowth();
        if (Starvation <= 0)
            GameManager.Instance.GameOver();
    }

    void HungerGrowth()
    {
        Starvation -= Time.deltaTime * HungerSpeed;
    }

    void Eat()
    {
        Starvation = 100;
        if (Starvation > 100)
            Starvation = 100;
        if (distance_max < 100)
            distance_max += 5;
        else if (HungerSpeed < 2)
            HungerSpeed += 0.1f;
        Debug.Log("distance_max : " + distance_max);
        Debug.Log("HungerSpeed : " + HungerSpeed);
    }

    void OnTriggerEnter2D(Collider2D collider)
    {


        Player player = (Player)collider.GetComponent<Player>();

        if (player != null)
        {
            if (player.GiveFood())
            {
                GameManager.Instance.FindWithTag("TalkingInterface").GetComponent<TalkingInterface>().FeedIt();
                Eat();
            }
        }
    }

    public Vector2 GetPosition()
    {
        Vector3 position = transform.position;
        return new Vector2(position.x, position.y);
    }

    public int GetStarvation()
    {
        return ((int)Starvation);
    }

    public void Respawn()
    {
        var v = Random.insideUnitCircle;
        int distance_min = distance_max - (distance_max / 10);
        Vector2 point = GetPosition() + v.normalized * distance_min + v * (distance_max - distance_min);
        point = SpawnCollider.ClosestPoint(point);
        Vector3 position = transform.position;
        position.x = point.x;
        position.y = point.y;
        transform.position = position;
        Quaternion rotation = Random.rotation;
        rotation.x = 0;
        rotation.y = 0;
        transform.rotation = rotation;
    }
}
